import asyncio
from proxybroker import Broker

async def show(proxies):
    textfile = open("proxies.txt", "w")
    while True:
        proxy = await proxies.get()
        textfile.write(str(proxy.host) + ":" + str(proxy.port) + "\n")
        if proxy is None: break
        print("Found %s proxies" % proxy)

proxies = asyncio.Queue()
broker = Broker(proxies)

types = []
limit = input("How many proxies do you want?")

tasks = asyncio.gather(
    broker.find(types=["HTTPS"], limit=limit),
    show(proxies))

print("Free public proxies for your testing and cracking needs")
print("MADE BY @JADETYCOON")
print("Made using ProxyBroker module")

loop = asyncio.get_event_loop()
loop.run_until_complete(tasks)
